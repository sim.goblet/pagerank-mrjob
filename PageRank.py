#! /usr/bin/env python3

from mrjob.job import MRJob
from mrjob.step import MRStep

"""
Algorithme de map-reduce appliquer à un exemple de graphe de citations.
Nous avons réalisé ici un algorithme de PageRank à l'aide de la bibliothèque MrJob sous Python.

Contraintes principales :
- Utiliser la bibliothèque MrJob
- Utiliser la valeur c = 0.15 (changement possible par la suite)

Usage:
======
    python PageRank.py file.txt

    file.txt: un fichier décrivant un réseau de page web
    -> Chaque ligne correspond à un lien web : - le premier champ est le numéro de la page source
                                               - le deuxième celui de la page cible
"""

class PageRank(MRJob):
    global initial_weight
    global cout
    global nb_iter
    cout=0.15
    nb_iter=10

    '''
    Fonction de récupération des pages web dans le fichier texte (.txt)
    '''
    def recup_resend(self, _, line):
        line = line.split('\t')
        try:
            yield line[0], line[1]
        except:
            pass

    '''
    Fonction de récupération et constitution d'une liste à partir des données extraites de recup_resend()
    Et définition du nombre de noeux (ici il vaut 1 parce c'est le premier).
    '''
    def recup_and_list(self, key, values):
        yield key, list(values)
        yield "nb_node", 1

    '''
    Calcul du poids initial en fonction de la page (ici key)
    '''
    def set_w0(self, key, values):
        global initial_weight
        if key == 'nb_node':
            initial_weight = 1 / sum(values)
        else:
            yield key, *list(values)

    '''
    Créer une liste des poids initial (dictionnaire) et les page liés (liste) pour chaque page.
    Affectation du poids initial (w0) à chaque page.
    '''
    def set_nodew0(self, key, values):
        yield key, [{'weight': initial_weight}, list(values)]

    '''
    Re-définition des poids pour toutes les pages en fonction du nombre de liens.
    '''
    def nextrank1(self, key, values):
        w, liste = values
        for out in liste:
            yield out, w['weight'] / len(liste)
        yield key, values

    def nextrank2(self, key, values):
        yield key, list(values)

    '''
    On affecte chaque page d’un nouveau poids (processus que l'on itèrera)
    '''
    def nextrank3(self, key, values):
        values = values
        L = []
        val = 0
        for i in values:
            if type(i) == type(L):
                L = i
            else:
                val += i
        if len(L)!=0:
            L[0]['weight'] = cout * initial_weight + (1 - cout) * val
            yield key, L

    def sortie(self,key,values):
        yield key, values[0]['weight']
        
    def steps(self):
        return [
            MRStep(mapper=self.recup_resend, reducer=self.recup_and_list),
            MRStep(reducer=self.set_w0),
            MRStep(mapper=self.set_nodew0)]+[ MRStep(mapper=self.nextrank1, reducer=self.nextrank2),
            MRStep(mapper=self.nextrank3)]*nb_iter+[MRStep(mapper=self.sortie) #Multiplication = itérations du processus.
            ]
        


if __name__ == '__main__':
    PageRank.run()