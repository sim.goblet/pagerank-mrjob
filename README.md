PageRank - MrJob
=============================================================

### Projet de l'UE Bases de données avancées dans le cadre du Master 2 - Data Sciences, données biologiques et numériques.

Implémentation d'un algorithme du PageRank à l'aide de mrJOB.
=============================================================
## Le Pagerank c'est quoi ?

Le PageRank ou algorithme PageRank a été créé par les fondateurs de Google et il est essentiel au moteur de recherche Google.<br/>
Il attribue un poids à chaque page parcourue. Ce poids est directement proportionnel à la pertinence donnée de la page associée lors de la recherche Google.
<br/><br/>
Une page avec un poids plus élevée est considérée comme plus pertinente et est mieux classée par le moteur de recherche lors du rappel de recherche.
Ces poids sont calculés de manière itérative et l'achèvement de chaque itération dépend du nombre de pages impliquées.<br/>
De plus, il est important de noter qu'une page est concidérée comme importante si elle est beaucoup citer par d'autres.<br/>

## Qu'est-ce que c'est mrJOB ?

mrJOB est un package Python (créé par Yelp) qui peut être utilisé pour écrire des tâches. <br/>
C'est un outil vraiment soigné et simple pour écrire des tâches de type map/reduce en Python.

## Installations

1. Python : `pip install python`
2. mrJOB : `pip install mrjob`

## Exécution de l'algorithme

1. Déplacez l'ensemble de données extrait, les fichiers \*.py vers un nouveau dossier.
2. Depuis **Terminal**, *Exécutez* `python ./PageRank.py path_of_'reseau.txt'` ou `python ./PageRank.py path_of_'soc-Epinions1.txt'`


# English version

### Project of the UE Advanced Databases in the framework of the Master 2 - Data Sciences, biological and numerical data.

Implementation of a PageRank algorithm using mrJOB.
=============================================================
## What is the Pagerank ?

The PageRank or PageRank algorithm was created by the founders of Google and is essential to the Google search engine.<br/>
It assigns a weight to each page browsed. This weight is directly proportional to the given relevance of the associated page during the Google search.
<br/><br/>
A page with a higher weight is considered more relevant and is ranked higher by the search engine during the search recall.
These weights are calculated iteratively and the completion of each iteration depends on the number of pages involved.<br/>
In addition, it is important to note that a page is considered important if it is quoted a lot by others.<br/>

## What is mrJOB?

mrJOB is a Python package (created by Yelp) that can be used to write tasks. <br/>
It's a really neat and simple tool for writing map/reduce tasks in Python.

## Facilities

1. Python: `pip install python`
2. mrJOB : `pip install mrjob`

## Running the algorithm

1. Move the extracted data set, the \*.py files to a new folder.
2. From **Terminal**, *Run* `python PageRank.py path_of_'reseau.txt'` or `python PageRank.py path_of_'soc-Epinions1.txt'`
